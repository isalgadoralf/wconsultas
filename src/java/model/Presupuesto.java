package model;

import java.util.ArrayList;
import java.util.Date;

public class Presupuesto {

    private String descripcion;
    private Date fecha;
    private String Ubicacion;
    private Integer presupuestoID;
    private String cliente;
    private Integer usuarioID;
    private ArrayList<Deitpre> _detalle;
    public Presupuesto( ) { 
      }
    public Presupuesto(String descripcion,Date fecha,String Ubicacion,Integer presupuestoID,String cliente,Integer usuarioID){
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.Ubicacion = Ubicacion;
        this.presupuestoID = presupuestoID;
       
        this.usuarioID = usuarioID;
    }
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getUbicacion() {
        return Ubicacion;
    }

    public void setUbicacion(String Ubicacion) {
        this.Ubicacion = Ubicacion;
    }

    public Integer getPresupuestoID() {
        return presupuestoID;
    }

    public void setPresupuestoID(Integer presupuestoID) {
        this.presupuestoID = presupuestoID;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public Integer getUsuarioID() {
        return usuarioID;
    }

    public void setUsuarioID(Integer usuarioID) {
        this.usuarioID = usuarioID;
    }

  public void set_detalle(ArrayList<Deitpre> _detalle) {
        this._detalle = _detalle;
    }

    public ArrayList<Deitpre> get_detalle() {
        return _detalle;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return this.descripcion;
    }

}