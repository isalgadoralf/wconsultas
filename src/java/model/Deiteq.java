package model;

public class Deiteq {

    private Double cantidad;
    private Integer equiposID;
    private Integer itemID;

    public Deiteq( ) { 
      }
    public Deiteq(Double cantidad,Integer equiposID,Integer itemID){
        this.cantidad = cantidad;
        this.equiposID = equiposID;
        this.itemID = itemID;
    }
    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getEquiposID() {
        return equiposID;
    }

    public void setEquiposID(Integer equiposID) {
        this.equiposID = equiposID;
    }

    public Integer getItemID() {
        return itemID;
    }

    public void setItemID(Integer itemID) {
        this.itemID = itemID;
    }


}