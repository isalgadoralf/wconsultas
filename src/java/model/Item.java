package model;

public class Item {

    private String descripcion;
    private Double precio;
    private Integer itemID;
    private Integer umedidaID;
    private Integer categoriaID;
    private String color;
    private Integer tipo;

    public Item( ) { 
      }
    public Item(String descripcion,Double precio,Integer itemID,Integer umedidaID,Integer categoriaID,String color,Integer tipo){
        this.descripcion = descripcion;
        this.precio = precio;
        this.itemID = itemID;
        this.umedidaID = umedidaID;
        this.categoriaID = categoriaID;
        this.color = color;
        this.tipo = tipo;
    }
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Integer getItemID() {
        return itemID;
    }

    public void setItemID(Integer itemID) {
        this.itemID = itemID;
    }

    public Integer getUmedidaID() {
        return umedidaID;
    }

    public void setUmedidaID(Integer umedidaID) {
        this.umedidaID = umedidaID;
    }

    public Integer getCategoriaID() {
        return categoriaID;
    }

    public void setCategoriaID(Integer categoriaID) {
        this.categoriaID = categoriaID;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }


}