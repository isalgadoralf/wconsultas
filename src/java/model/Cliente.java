package model;

public class Cliente {

    private String apellidos;
    private String nombres;
    private String telefonos;
    private Integer clienteID;

    public Cliente( ) { 
      }
    public Cliente(String apellidos,String nombres,String telefonos,Integer clienteID){
        this.apellidos = apellidos;
        this.nombres = nombres;
        this.telefonos = telefonos;
        this.clienteID = clienteID;
    }
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getTelefonos() {
        return telefonos;
    }

    public void setTelefonos(String telefonos) {
        this.telefonos = telefonos;
    }

    public Integer getClienteID() {
        return clienteID;
    }

    public void setClienteID(Integer clienteID) {
        this.clienteID = clienteID;
    }


}