package model;

public class Deitmo {

    private Double cantidad;
    private Integer mobraID;
    private Integer itemID;

    public Deitmo( ) { 
      }
    public Deitmo(Double cantidad,Integer mobraID,Integer itemID){
        this.cantidad = cantidad;
        this.mobraID = mobraID;
        this.itemID = itemID;
    }
    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getMobraID() {
        return mobraID;
    }

    public void setMobraID(Integer mobraID) {
        this.mobraID = mobraID;
    }

    public Integer getItemID() {
        return itemID;
    }

    public void setItemID(Integer itemID) {
        this.itemID = itemID;
    }


}