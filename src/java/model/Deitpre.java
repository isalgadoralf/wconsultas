package model;

public class Deitpre {

    private Double cantidad;
    private Double total;
    private Integer presupuestoID;
    private Integer itemID;

    public Deitpre( ) { 
      }
    public Deitpre(Double cantidad,Double total,Integer presupuestoID,Integer itemID){
        this.cantidad = cantidad;
        this.total = total;
        this.presupuestoID = presupuestoID;
        this.itemID = itemID;
    }
    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Integer getPresupuestoID() {
        return presupuestoID;
    }

    public void setPresupuestoID(Integer presupuestoID) {
        this.presupuestoID = presupuestoID;
    }

    public Integer getItemID() {
        return itemID;
    }

    public void setItemID(Integer itemID) {
        this.itemID = itemID;
    }


}