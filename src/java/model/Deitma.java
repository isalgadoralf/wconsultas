package model;

public class Deitma {

    private Double cantidad;
    private Integer materialesID;
    private Integer itemID;

    public Deitma( ) { 
      }
    public Deitma(Double cantidad,Integer materialesID,Integer itemID){
        this.cantidad = cantidad;
        this.materialesID = materialesID;
        this.itemID = itemID;
    }
    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getMaterialesID() {
        return materialesID;
    }

    public void setMaterialesID(Integer materialesID) {
        this.materialesID = materialesID;
    }

    public Integer getItemID() {
        return itemID;
    }

    public void setItemID(Integer itemID) {
        this.itemID = itemID;
    }


}