package model;

public class Umedida {

    private String abreviatura;
    private String descripcionn;
    private Integer umedidaID;

    public Umedida( ) { 
      }
    public Umedida(String abreviatura,String descripcionn,Integer umedidaID){
        this.abreviatura = abreviatura;
        this.descripcionn = descripcionn;
        this.umedidaID = umedidaID;
    }
    public String getAbreviatura() {
        return abreviatura;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    public String getDescripcionn() {
        return descripcionn;
    }

    public void setDescripcionn(String descripcionn) {
        this.descripcionn = descripcionn;
    }

    public Integer getUmedidaID() {
        return umedidaID;
    }

    public void setUmedidaID(Integer umedidaID) {
        this.umedidaID = umedidaID;
    }


}