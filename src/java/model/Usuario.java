package model;

public class Usuario {

    private String apellidos;
    private String login;
    private String nombres;
    private String pass;
    private String telefonos;
    private Integer usuarioID;

    public Usuario( ) { 
      }
    public Usuario(String apellidos,String login,String nombres,String pass,String telefonos,Integer usuarioID){
        this.apellidos = apellidos;
        this.login = login;
        this.nombres = nombres;
        this.pass = pass;
        this.telefonos = telefonos;
        this.usuarioID = usuarioID;
    }
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getTelefonos() {
        return telefonos;
    }

    public void setTelefonos(String telefonos) {
        this.telefonos = telefonos;
    }

    public Integer getUsuarioID() {
        return usuarioID;
    }

    public void setUsuarioID(Integer usuarioID) {
        this.usuarioID = usuarioID;
    }


}