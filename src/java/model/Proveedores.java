package model;

public class Proveedores {

    private String descripcion;
    private String Telefonos;
    private String ubicacion;
    private Integer categoriaID;
    private Integer proveedoresID;

    public Proveedores( ) { 
      }
    public Proveedores(String descripcion,String Telefonos,String ubicacion,Integer categoriaID,Integer proveedoresID){
        this.descripcion = descripcion;
        this.Telefonos = Telefonos;
        this.ubicacion = ubicacion;
        this.categoriaID = categoriaID;
        this.proveedoresID = proveedoresID;
    }
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTelefonos() {
        return Telefonos;
    }

    public void setTelefonos(String Telefonos) {
        this.Telefonos = Telefonos;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Integer getCategoriaID() {
        return categoriaID;
    }

    public void setCategoriaID(Integer categoriaID) {
        this.categoriaID = categoriaID;
    }

    public Integer getProveedoresID() {
        return proveedoresID;
    }

    public void setProveedoresID(Integer proveedoresID) {
        this.proveedoresID = proveedoresID;
    }


}