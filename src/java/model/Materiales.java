package model;

public class Materiales {

    private String Descripcion;
    private Double precio;
    private Integer materialesID;
    private Integer umedidaID;

    public Materiales( ) { 
      }
    public Materiales(String Descripcion,Double precio,Integer materialesID,Integer umedidaID){
        this.Descripcion = Descripcion;
        this.precio = precio;
        this.materialesID = materialesID;
        this.umedidaID = umedidaID;
    }
    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Integer getMaterialesID() {
        return materialesID;
    }

    public void setMaterialesID(Integer materialesID) {
        this.materialesID = materialesID;
    }

    public Integer getUmedidaID() {
        return umedidaID;
    }

    public void setUmedidaID(Integer umedidaID) {
        this.umedidaID = umedidaID;
    }


}