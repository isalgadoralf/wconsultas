package model;

public class Mobra {

    private String descripcion;
    private Double precio;
    private Integer mobraID;
    private Integer umedidaID;

    public Mobra( ) { 
      }
    public Mobra(String descripcion,Double precio,Integer mobraID,Integer umedidaID){
        this.descripcion = descripcion;
        this.precio = precio;
        this.mobraID = mobraID;
        this.umedidaID = umedidaID;
    }
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Integer getMobraID() {
        return mobraID;
    }

    public void setMobraID(Integer mobraID) {
        this.mobraID = mobraID;
    }

    public Integer getUmedidaID() {
        return umedidaID;
    }

    public void setUmedidaID(Integer umedidaID) {
        this.umedidaID = umedidaID;
    }


}