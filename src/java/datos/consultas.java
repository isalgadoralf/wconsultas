/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import model.Deitpre;
import model.Item;
import model.Presupuesto;
import model.Proveedores;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.apache.commons.codec.binary.Base64;
import util.Constantes;

/**
 *
 * @author Rafael
 */
@WebService(serviceName = "consultas")
public class consultas {

    private Gson gson = new Gson();

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }

    /**
     * Web service operation
     *
     * @param a
     * @param b
     * @return
     */
    @WebMethod(operationName = "Suma")
    public int Suma(@WebParam(name = "a") int a, @WebParam(name = "b") int b) {
        //TODO write your implementation code here:
        return a + b;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getProveedores")
    public String getProveedores() {
        //TODO write your implementation code here:
        Proveedores p = new Proveedores();
        ManejadorDB m = ManejadorDB.getManejador();
        ArrayList<Object> lista = m.getLista(p);
        ArrayList<Proveedores> lp = new ArrayList<Proveedores>();
        ManejadorDB.convertir(lp, lista);
        // ## Serializamos el resultado como string json
        String liString = gson.toJson(lp);
        System.out.println(liString);
        return liString;
    }

    @WebMethod(operationName = "getItems")
    public String getItems() {
        //TODO write your implementation code here:
        Item p = new Item();
        ManejadorDB m = ManejadorDB.getManejador();
        ArrayList<Object> lista = m.getLista(p);
        ArrayList<Item> lp = new ArrayList<Item>();
        ManejadorDB.convertir(lp, lista);
        // ## Serializamos el resultado como string json
        String liString = gson.toJson(lp);
        System.out.println(liString);
        return liString;
    }

    @WebMethod(operationName = "setPresupuesto")
    public String setPresupuesto(@WebParam(name = "pre") String pre) {
        //TODO write your implementation code here:
       Gson gsonn = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
     
        System.out.println(pre);
        Presupuesto pedi = gsonn.fromJson(pre, Presupuesto.class);
        int res = ManejadorDB.guardarID(pedi);
        if(res>0){
            guardarDetalle(pedi);
            System.out.println("SE GUARDO PRESUPUESTO");
        }
       /* int respeusta = pedidoDAO.GudarPedido(pedi);
        socketCliente.sendMessage("1");*/
        
        return ""+res;
    }
    @WebMethod(operationName = "getPresupuesto")
    public String getPresupuesto(@WebParam(name = "idpre") int idpre) {
        try {
            //TODO write your implementation code here:
            System.out.println(idpre);
            Constantes.pdf(idpre);
            File jasper = new File("E:\\temporal\\reporte.pdf");
            byte[] arreglo = Constantes.getBytesFromFile(jasper);
            String salida  = Base64.encodeBase64String(arreglo);
            System.out.println(salida);
           /*  File xx = new File("E:\\reporte.pdf");
            FileOutputStream fos = new FileOutputStream(xx);
            fos.write(arreglo);
            fos.close();*/
            
            
            return salida;
        } catch (IOException ex) {
            Logger.getLogger(consultas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(consultas.class.getName()).log(Level.SEVERE, null, ex);
        }
         return "";
    }
    @WebMethod(operationName = "getMat")
    public String getMat() {
        try {
            //TODO write your implementation code here:
            System.out.println();
            Constantes.pdfMat();
            File jasper = new File("/home/ubuntu/temporal/mat.pdf");
       //     File jasper = new File("E:\\temporal\\mat.pdf");
            byte[] arreglo = Constantes.getBytesFromFile(jasper);
            String salida  = Base64.encodeBase64String(arreglo);
            System.out.println(salida);
           /*  File xx = new File("E:\\reporte.pdf");
            FileOutputStream fos = new FileOutputStream(xx);
            fos.write(arreglo);
            fos.close();*/
            
            
            return salida;
        } catch (IOException ex) {
            Logger.getLogger(consultas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(consultas.class.getName()).log(Level.SEVERE, null, ex);
        }
         return "";
    }
    private void guardarDetalle(Presupuesto pedi){
        ArrayList<Deitpre> lista =   pedi.get_detalle();
        ManejadorDB mm = new ManejadorDB();
        for (int i = 0; i < lista.size(); i++) {
            Deitpre e = lista.get(i);
            mm.guardarID(e);
        }
        
    }
    
    
   
}
