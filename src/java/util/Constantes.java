/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import datos.DBHelper;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 *
 * @author Administrador
 */
public class Constantes {

    public static String DIRECTORIO = "model";
    public static String TAB_ATRIBUTO = "    ";
    public static String DATE = "import java.util.Date;";

    public static String nombreClase(String nombre) {

        String cabecera = "public class "
                + Constantes.convertirPrimero(nombre) + " {";

        return cabecera;
    }

    public static String getAtributo(String tipo, String nombre) {
        if (tipo.equalsIgnoreCase("TINYINT")) {
            tipo = "BOOL";
        }
        return Constantes.TAB_ATRIBUTO + "private " + Constantes.getTipo(tipo) + " " + nombre + ";";

    }

    public static String convertirPrimero(String nombre) {
        String aux = nombre.substring(0, 1).toUpperCase();
        return aux + nombre.substring(1, nombre.length());
    }

    public static String getTipo(String t) {
        if (t.equalsIgnoreCase("INT")) {
            return "Integer";
        } else if (t.equalsIgnoreCase("DATE") || t.equalsIgnoreCase("DATETIME")) {
            return "Date";
        } else if (t.equalsIgnoreCase("VARCHAR")) {
            return "String";
        } else if (t.equalsIgnoreCase("TIME")) {
            return "Date";
        } else if (t.equalsIgnoreCase("CHAR")) {
            return "char";

        } else if (t.equalsIgnoreCase("DOUBLE")) {
            return "Double";
        } else if (t.equalsIgnoreCase("BOOL")) {
            return "Boolean";
        } else {
            return "";
        }

    }

    public static String obtenerGet(String atributo) {

        String[] datos = atributo.split(" ");
        String nom = datos[6].substring(0, datos[6].length() - 1);
        String tipo = datos[5];
        String aux = Constantes.TAB_ATRIBUTO + "public " + tipo + " get" + Constantes.convertirPrimero(nom) + "() {" + "\n";
        aux += Constantes.TAB_ATRIBUTO + Constantes.TAB_ATRIBUTO + "return " + nom + ";" + "\n";
        aux += Constantes.TAB_ATRIBUTO + "}";
        return aux;
    }

    public static String obtenerSet(String atributo) {
        /*
         public void setAlmacenId(Integer almacenId) {
         this.almacenId = almacenId;
         }
         */
        String[] datos = atributo.split(" ");
        String nom = datos[6].substring(0, datos[6].length() - 1);
        String tipo = datos[5];
        String aux = Constantes.TAB_ATRIBUTO + "public void set" + Constantes.convertirPrimero(nom) + "(" + tipo + " " + nom + ") {" + "\n";
        aux += Constantes.TAB_ATRIBUTO + Constantes.TAB_ATRIBUTO + "this." + nom + " = " + nom + ";\n";
        aux += Constantes.TAB_ATRIBUTO + "}";
        return aux;
    }

    public static byte[] getBytesFromFile(File file) throws IOException {
        // Get the size of the file
        long length = file.length();

        // You cannot create an array using a long type.
        // It needs to be an int type.
        // Before converting to an int type, check
        // to ensure that file is not larger than Integer.MAX_VALUE.
        if (length > Integer.MAX_VALUE) {
            // File is too large
            throw new IOException("File is too large!");
        }

        // Create the byte array to hold the data
        byte[] bytes = new byte[(int) length];

        // Read in the bytes
        int offset = 0;
        int numRead = 0;

        InputStream is = new FileInputStream(file);
        try {
            while (offset < bytes.length
                    && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
                offset += numRead;
            }
        } finally {
            is.close();
        }

        // Ensure all the bytes have been read in
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }
        return bytes;
    }

    public static void pdf(int idpresu) throws SQLException {
        JasperReport jasperReport;
        JasperPrint jasperPrint;
        try {
            //se carga el reporte
            //  URL  in=this.getClass().getResource( "reporte.jasper" );
            File jasper = new File("E:\\PROYECTOS\\SW\\FINAL\\PROYECTO\\WSERVER\\WConsultas\\web\\rpt\\report1.jasper");
            Map parameters = new HashMap();
            parameters.put("idpresu", idpresu);
            jasperReport = (JasperReport) JRLoader.loadObject(jasper);
            //se procesa el archivo jasper
            Connection cc = DBHelper.getConnection();
            jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, cc);
            //se crea el archivo PDF
            JasperExportManager.exportReportToPdfFile(jasperPrint, "E:/temporal/reporte.pdf");
        } catch (JRException ex) {
            System.err.println("Error iReport: " + ex.getMessage());
        } catch (SQLException ex) {
            Logger.getLogger(Constantes.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Constantes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void pdfMat() throws SQLException {
        JasperReport jasperReport;
        JasperPrint jasperPrint;
        try {
          
            File jasper = new File("/home/ubuntu/rpt/report4.jasper");
         //   File jasper = new File("E:\\rpt\\report4.jasper");
            Map parameters = new HashMap();
        //    parameters.put("idpresu", idpresu);
            jasperReport = (JasperReport) JRLoader.loadObject(jasper);
            //se procesa el archivo jasper
            Connection cc = DBHelper.getConnection();
            jasperPrint = JasperFillManager.fillReport(jasperReport, null, cc);
            //se crea el archivo PDF
            JasperExportManager.exportReportToPdfFile(jasperPrint, "/home/ubuntu/temporal/mat.pdf");
          //  JasperExportManager.exportReportToPdfFile(jasperPrint, "E:/temporal/mat.pdf");
        } catch (JRException ex) {
            System.err.println("Error iReport: " + ex.getMessage());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Constantes.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Constantes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
